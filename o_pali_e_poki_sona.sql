DROP DATABASE IF EXISTS musi_pi_ilo_toki;

CREATE DATABASE musi_pi_ilo_toki
	CHARACTER SET utf8mb4
	COLLATE utf8mb4_general_ci;

USE musi_pi_ilo_toki;

SET NAMES utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE jan_kepeken (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nimi VARCHAR(255),
	kule BINARY(3) NOT NULL DEFAULT 0,
	sitelen CHAR(4) NOT NULL DEFAULT 'Latn',
	tomo_linluwi VARCHAR(320) NOT NULL UNIQUE,
	/*namako CHAR(16) NOT NULL,*/
	nimi_open CHAR(60) CHARACTER SET ascii COLLATE ascii_bin NOT NULL
);

CREATE TABLE toki_pi_jan_kepeken (
	ID_pi_jan_kepeken INT NOT NULL, FOREIGN KEY (ID_pi_jan_kepeken) REFERENCES jan_kepeken(ID),
	toki CHAR(3) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
	UNIQUE (ID_pi_jan_kepeken,toki)
);

CREATE TABLE musi (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	pake_pi_jan_kepeken INT, FOREIGN KEY (pake_pi_jan_kepeken) REFERENCES jan_kepeken(ID),
	tenpo_musi DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	toki_lon CHAR(3) CHARACTER SET ascii COLLATE ascii_bin
);

CREATE TABLE jaki_tawa_jan_kepeken (
	ID_pi_jan_kepeken INT NOT NULL, FOREIGN KEY (ID_pi_jan_kepeken) REFERENCES jan_kepeken(ID),
	ID_musi INT NOT NULL, FOREIGN KEY (ID_musi) REFERENCES musi(ID)
);

CREATE TABLE kipisi_musi (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ID_musi INT NOT NULL, FOREIGN KEY (ID_musi) REFERENCES musi(ID),
	ID_pi_jan_kepeken INT NOT NULL,	FOREIGN KEY (ID_pi_jan_kepeken) REFERENCES jan_kepeken(ID),
	nimi_mute TEXT NOT NULL,
	toki CHAR(3) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
	jaki INT NOT NULL DEFAULT 0
);

CREATE TABLE poki_toki ( /* lukin taso */
	toki CHAR(3) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
	nimi_toki VARCHAR(32) NOT NULL
);

INSERT INTO poki_toki (toki, nimi_toki) VALUES
	('afh', 'El-Afrihili'),
	('afr', 'Afrikaans'),
	('avk', 'Kotava'),
	('ara', 'العَرَبِيَّة'),
	('ben', 'বাংলা'),
	('bul', 'български език'),
	('cat', 'català'),
	('ceb', 'Sinugbuanong Binisayâ'),
	('ces', 'čeština'),
	('cym', 'Cymraeg'),
	('cym', 'y Gymraeg'),
	('dan', 'dansk'),
	('deu', 'Deutsch'),
	('ell', 'Ελληνικά'),
	('eng', 'English'),
	('epo', 'Esperanto'),
	('fas', 'فارسی'),
	('fin', 'suomen kieli'),
	('fra', 'français'),
	('gle', 'Gaeilge'),
	('hin', 'हिन्दी'),
	('hun', 'magyar nyelv'),
	('ido', 'Ido'),
	('ile', 'Interlingue'),
	('ile', 'Occidental'),
	('ila', 'interlingua'),
	('ind', 'bahasa Indonesia'),
	('ita', 'italiano'),
	('jbo', 'la .lojban.'),
	('jpn', '日本語'),
	('kor', '한국어'),
	('lat', 'lingua latīna'),
	('ldn', 'Láadan'),
	('lfn', 'lingua franca nova'),
	('lfn', 'elefen'),
	('lfn', 'лингуа франка нова'),
	('lfn', 'елефен'),
	('mri', 'Te Reo Māori'),
	('nld', 'Nederlands'),
	('nor', 'norsk'),
	('nov', 'novial'),
	('pol', 'polski'),
	('por', 'português'),
	('qis', 'medžuslovjansky'),
	('qis', 'меджусловјанскы'),
	('qit', 'Iţkuîl'),
	('qnl', 'neolatino'),
	('qtv', 'Viossa'),
	('ron', 'limba română'),
	('rus', 'русский'),
	('spa', 'español'),
	('swe', 'svenska'),
	('tgl', 'Wikang Tagalog'),
	('tha', 'ภาษาไทย'),
	('tlh', 'tlhIngan-Hol'),
	('tur', 'Türkçe'),
	('ukr', 'Українська мова'),
	('vol', 'Volapük'),
	('zho', '中文');
