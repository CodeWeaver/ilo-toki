<?php
session_start();
require_once 'o_lukin_e_jan_kepeken.php';
require_once 'o_open_e_poki_sona.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (isset($_POST['pini'])) {
		session_destroy();
		header('Location: open_pi_jan_kepeken.php', true, 303);
		exit();
	}

	$poki_sona
		->prepare('UPDATE jan_kepeken SET sitelen=?,nimi=?,kule=UNHEX(?) WHERE ID=?;')
		->execute([$_POST['sitelen'], $_POST['nimi'], ltrim($_POST['kule'], '#'), $_SESSION['ID']]);

	if (isset($_POST['weka']))
		$poki_sona
			->prepare('DELETE FROM toki_pi_jan_kepeken WHERE ID_pi_jan_kepeken=? AND toki=?;')
			->execute([$_SESSION['ID'], $_POST['weka']]);

	if ($_POST['toki']) {
		$toki_pana = array();
		$lon = preg_match('/\(([a-z]{3})\)/', $_POST['toki'], $toki_pana)
			?? preg_match('/^([A-Za-z]{3})/', $_POST['toki'], $toki_pana);
		if ($lon)
			$poki_sona
				->prepare('INSERT INTO toki_pi_jan_kepeken (ID_pi_jan_kepeken,toki) VALUES (?,?);')
				->execute([$_SESSION['ID'], strtolower($toki_pana[1])]);
	}
}

$seme_a = $poki_sona->prepare('SELECT sitelen,nimi,HEX(kule) AS kule FROM jan_kepeken WHERE ID=?;');
$seme_a->execute([$_SESSION['ID']]);
$jan = $seme_a->fetch();

?>
<html>
	<?php include 'insa_insa.php' ?>
	<body>
		<form method="post">
			<div style="text-align:right">
				<a href="lipu_open.php">tawa lipu open</a>
				<input name="pini" type="submit" value="pini o">
			</div>
			<hr><br>
			<label for="nimi">nimi sina</label>
			<input name="nimi" id="nimi" type="text" value="<?php echo $jan['nimi'] ?>">
			&emsp;
			<label for="kule">kule</label>
			<input name="kule" id="kule" type="color" value="<?php echo '#'.$jan['kule'] ?>">

			<!--fieldset>
				<legend>nasin sitelen</legend>
				< nimi li nasa tan nasin HTML >
				<ul>
				<li><input name="sitelen" id="sitelen-Latn" type="radio" value="Latn" <?php if (!$jan['sitelen'] or $jan['sitelen']=='Latn') echo 'checked'; ?>><label for="sitelen-Latn">
						toki pona</label>
					</li>
					<li><input name="sitelen" id="sitelen-Qaap" type="radio" value="Qaap" <?php if ($jan['sitelen']=='Qaap') echo 'checked'; ?>><label for="sitelen-Qaap" style="font-family:'linja pona';">
						&nbsp;toki+pona</label>
					</li>
					<li><input name="sitelen" id="sitelen-Hrkt" type="radio" value="Hrkt" <?php if ($jan['sitelen']=='Hrkt') echo 'checked'; ?>><label for="sitelen-Hrkt">
						トキポナ</label>
					</li>
				</ul>
			</fieldset-->
			<br>
			<br>
			<label for="toki">toki sina</label>
			<input name="toki" type="text" list="toki-mute">
			<datalist id="toki-mute">
				<?php
				$seme_a = $poki_sona->query('SELECT nimi_toki,toki FROM poki_toki ORDER BY toki;');
				while ($kipisi = $seme_a->fetch())
					echo "<option>{$kipisi['nimi_toki']} ({$kipisi['toki']})</option>";
				?>
			</datalist>
			<input type="submit" value="pana">
			<fieldset>
				<ul>
					<li>
						<button class="x" style="visibility:hidden">X</button>
						(qtp) toki pona
					</li>
					<?php
					$seme_a = $poki_sona->prepare(<<<'SQL'
						SELECT nimi_toki,tu.toki AS toki
						FROM poki_toki AS wan
						RIGHT JOIN toki_pi_jan_kepeken AS tu ON wan.toki=tu.toki
						WHERE tu.ID_pi_jan_kepeken=?
						ORDER BY tu.toki;
						SQL
					);
					$seme_a->execute([$_SESSION['ID']]);
					while ($kipisi = $seme_a->fetch())
						$HTML .= <<<HTML
							<li>
								<button class="x" name="weka" type="submit" value="{$kipisi['toki']}">X</button>
								({$kipisi['toki']}) {$kipisi['nimi_toki']}
							</li>
							HTML;
					if ($HTML)
						echo $HTML;
					else
						echo '<li>sina o jo e wan anu mute!</li>';
					?>
				</ul>
			</fieldset>
			<br>
			<input type="submit" value="awen">
		</form>

		<?php include 'anpa.php'; ?>
	</body>
</html>
