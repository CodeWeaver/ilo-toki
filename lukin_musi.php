<?php session_start(); ?>
<html>
	<?php include 'insa_insa.php' ?>
	<style>
	th {
		text-align: right;
	}
	td {
		text-align: left;
	}
	</style>
	<script>
	function o_len_ala(ID) {
		document.getElementById("len-"+ID).style.display = 'none';
		document.getElementById(ID).style.display = 'initial';
	}
	function tan_pakala(ID) {
		if (confirm("toki ni li jo ala jo e nimi jaki?")) {
			let http = new XMLHttpRequest();
			http.open('POST', 'pakala.php', true);
			http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			http.send('pakala=nimi jaki&kipisi='+ID);
			document.getElementById(ID).style.display = 'none';
		}
	}
	</script>
	<body>
		<?php 
		require_once 'o_open_e_poki_sona.php';

		function o_kama_jo_e_musi($ID_musi) {
			global $poki_sona;
			$seme_a = $poki_sona->prepare(<<<SQL
				SELECT
					kipisi_musi.ID AS ID,
					nimi_mute,
					toki,
					jan.nimi AS nimi,
					HEX(kule) AS kule,
					jaki
				FROM kipisi_musi
				LEFT JOIN jan_kepeken AS jan ON jan.ID=ID_pi_jan_kepeken
				WHERE ID_musi=?
				ORDER BY kipisi_musi.ID;
				SQL
			);
			$seme_a->execute([$ID_musi]);

			$jaki = 2;
			echo '<table>';
			while ($kipisi = $seme_a->fetch()) {
				if ($kipisi['jaki'] >= $jaki)
					echo <<<HTML
						<tr id="len-{$kipisi['ID']}">
							<td colspan=4>
								ni li jo e nimi jaki.
								<a onclick="o_len_ala({$kipisi['ID']})" style="cursor:pointer;color:blue;text-decoration:underline">
									o len ala
								</a>
							</td>
						</tr>
						<tr id="{$kipisi['ID']}" style="display:none">
						HTML;
				else
					echo "<tr id=\"{$kipisi['ID']}\">";
				echo <<<HTML
						<th style="color:#{$kipisi['kule']}">{$kipisi['nimi']}</th>
						<td>{$kipisi['toki']}</td>
						<td>{$kipisi['nimi_mute']}</td>
						<td><a onclick="tan_pakala({$kipisi['ID']})" style="cursor:pointer">&#x1F6A9;</a></td>
					</tr>
					HTML;
			}
			echo '</table>';
		}

		if (isset($_GET['musi']))
			o_kama_jo_e_musi($_GET['musi']);

		else {
			$seme_a = $poki_sona->prepare(<<<SQL
				SELECT DISTINCT ID_musi
				FROM kipisi_musi
				WHERE ID_pi_jan_kepeken=?
				ORDER BY ID_musi DESC;
				SQL
			);
			$seme_a->execute([$_GET['jan'] ?? $_SESSION['ID']]);
			while ($ID_musi = $seme_a->fetchColumn()) {
				echo "<fieldset><legend>musi nanpa $ID_musi</legend>";
				o_kama_jo_e_musi($ID_musi);
				echo '</fieldset>';
			}
		}

		include 'anpa.php';
		?>
	</body>
</html>
