<?php
if (isset($_POST['tawa'])) {
	if ($_POST['nimi-linluwi'] && $_POST['nimi-open']) {
		// open pi poki sona
		require_once 'o_open_e_poki_sona.php';
		#$pakala .= '<li>mi ken ala open e poki sona.</li>';

		// jan kepeken ni li lon ala lon?
		$seme_a = $poki_sona->prepare('SELECT ID,nimi_open FROM jan_kepeken WHERE tomo_linluwi=?;');
		$seme_a->execute([$_POST['nimi-linluwi']]);
		$lon = $seme_a->fetch();

		if ($lon) {	// kama toki
			if (password_verify($_POST['nimi-open'], $lon['nimi_open'])) {
				session_start();
				$_SESSION['ID'] = $lon['ID'];
				header('Location: lipu_open.php', true, 303);
				exit();
			} else
				$pakala .= '<li>nimi open ike!</li>';

		} else {	// jan kepeken sin
			$seme_a = $poki_sona->prepare('INSERT INTO jan_kepeken (tomo_linluwi,nimi_open) VALUES (?,?);');
			$seme_a->execute([$_POST['nimi-linluwi'], password_hash($_POST['nimi-open'], PASSWORD_BCRYPT)]);
			$seme_a = $poki_sona->query('SELECT LAST_INSERT_ID();');
			$ID = $seme_a->fetchColumn();
			//session_cache_limiter('');	// session o pana ala e nimi lawa
			session_start();
			$_SESSION['ID'] = $ID;
			session_write_close();
			header('Location: lipu_pi_jan_kepeken.php', true, 303);
			exit();
		}
	}
	else {
		if (!$_POST['nimi-linluwi'])
			$pakala .= '<li>nimi linluwi li weka!</li>';
		if (!$_POST['nimi-open'])
			$pakala .= '<li>nimi open li weka!</li>';
	}
}
?>
<html>
	<?php include 'insa_insa.php' ?>
	<style>
	* {
		font-size: 1em;
	}
	td {
		border: none;
	}
	</style>
	<body>
		<?php if ($pakala) echo "<ul style=\"pakala\">$pakala</ul>"; ?>
		<form method="post">
			<table>
				<tr>
					<td><label for="nimi-linluwi">nimi linluwi</label></td>
					<td><input name="nimi-linluwi" id="nimi-linluwi" type="email" maxlength="320" placeholder="jan@example.com"></td>
				</tr>
				<tr>
					<td><label for="nimi-open">nimi open</label></td>
					<td><input name="nimi-open" id="nimi-open" type="password" maxlength="72" placeholder="••••••••"></td>
					<td><input type="submit" name="tawa" value="tawa"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
