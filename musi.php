<?php
session_start();
require_once 'o_lukin_e_jan_kepeken.php';
require_once 'o_open_e_poki_sona.php';

$seme_a = $poki_sona->prepare('SELECT * FROM toki_pi_jan_kepeken WHERE ID_pi_jan_kepeken=?;');
$seme_a->execute([$_SESSION['ID']]);
if ($seme_a->fetch() == NULL) {
	header('Location: lipu_pi_jan_kepeken.php', true, 303);
	exit();
}

// alasa pi musi lon
$seme_a = $poki_sona->prepare(<<<SQL
	SELECT ID FROM musi
	WHERE
		(
			CURRENT_TIMESTAMP > ADDTIME(tenpo_musi,'00:24:00')
			OR pake_pi_jan_kepeken = :ID
			OR pake_pi_jan_kepeken IS NULL
		)
		AND ID NOT IN (
			SELECT ID_musi FROM kipisi_musi
			WHERE ID_pi_jan_kepeken = :ID
		)
		AND ID NOT IN (
			SELECT ID_musi FROM jaki_tawa_jan_kepeken
			WHERE ID_pi_jan_kepeken = :ID
		)
		AND (
			toki_lon = 'qtp'
			OR toki_lon IN (
				SELECT toki FROM toki_pi_jan_kepeken
				WHERE ID_pi_jan_kepeken = :ID
			)
		)
	ORDER BY tenpo_musi
	LIMIT 1;
	SQL
);
$seme_a->execute(['ID' => $_SESSION['ID']]);
$ID_musi = $seme_a->fetchColumn();
if (!$ID_musi) {
	$seme_a = $poki_sona->prepare(<<<SQL
		INSERT INTO musi (pake_pi_jan_kepeken)
		VALUES (?);
		SQL
	);
	$seme_a->execute([$_SESSION['ID']]);

	$seme_a = $poki_sona->query('SELECT LAST_INSERT_ID();');
	$ID_musi = $seme_a->fetchColumn();
} else
	$poki_sona
		->prepare('UPDATE musi SET pake_pi_jan_kepeken=? WHERE ID=?;')
		->execute([$_SESSION['ID'], $ID_musi]);

$seme_a = $poki_sona->prepare(<<<SQL
	SELECT ID,nimi_mute,toki
	FROM kipisi_musi
	WHERE ID_musi=?
	ORDER BY ID DESC
	LIMIT 1;
	SQL
);
$seme_a->execute([$ID_musi]);
$kipisi_musi = $seme_a->fetch();
?>
<html>
	<?php include 'insa_insa.php'; ?>
	<body>
	<?php if ($kipisi_musi['toki']): ?>
		<?= $kipisi_musi['nimi_mute'] ?>
		<a onclick="document.getElementById('tan-pakala').style.display='initial';" style="cursor:pointer">&#x1F6A9;</a>
		<fieldset id="tan-pakala" style="display:none">
			<legend>pana e sona tan pakala</legend>
			<form method="post" action="pakala.php">
				<input name="kipisi" type="hidden" value="<?= $kipisi_musi['ID'] ?>">
				<ul>
					<li><input name="pakala" id="pakala-wan" type="radio" value="nimi jaki"><label for="pakala-wan">
						nimi jaki anu ike</label></li>
					<li><input name="pakala" id="pakala-tu" type="radio" value="toki pakala"><label for="pakala-tu">
						mi toki ala e toki ni</label></li>
				</ul>
				<button type="button" onclick="document.getElementById('tan-pakala').style.display='none';">pini</button>
				<input type="submit" value="pana">
			</form>
		</fieldset>
	<?php else: ?>
		<h3>musi sin a! o toki e sin, anu kepeken nimi ni tan ilo!</h3>
		<br>
		<?= trim(`racket o_toki_pali.rkt`, "\"\n"); ?>
	<?php endif; ?>
		<br>
		<br>
		<form method="post" action="pana.php">
			<input name="ID-musi" type="hidden" value="<?= $ID_musi; ?>">
			<label for="toki">toki</label>
		<?php if (!$kipisi_musi['toki'] or $kipisi_musi['toki'] == 'qtp'): ?>
			<select name="toki" id="toki-wile" required>
				<?php
				if (!$kipisi_musi['toki'])
					echo '<option>toki pona (qtp) [jan toki ale]</option>';

				$seme_a = $poki_sona->prepare(<<<SQL
					SELECT
						nimi_toki,
						tu.toki AS toki,
						(
							SELECT COUNT(*)
							FROM toki_pi_jan_kepeken AS san
							WHERE san.toki=wan.toki
						) AS nanpa
					FROM poki_toki AS wan
					RIGHT JOIN toki_pi_jan_kepeken AS tu ON wan.toki=tu.toki
					WHERE tu.ID_pi_jan_kepeken=?
					ORDER BY tu.toki
					SQL
				);
				$seme_a->execute([$_SESSION['ID']]);
				while ($kipisi = $seme_a->fetch()) {
					switch ($kipisi['nanpa'] - 1) {
						case -1:
						case 0: $nanpa = 'ala'; break;
						case 1: $nanpa = 'wan'; break;
						case 2: $nanpa = 'tu'; break;
						default:
							if ($kipisi['nanpa'] < 10)
								$nanpa = 'luka';
							else
								$nanpa = 'mute';
							break;
					}
					echo "<option>{$kipisi['nimi_toki']} ({$kipisi['toki']}) [jan toki $nanpa]</option>";
				}
				?>
			</select>
		<?php else: ?>
			<input name="toki" type="hidden" value="toki pona (qtp) [jan toki ale]">
		<?php endif; ?>
			<br>
			<textarea name="toki-sin" id="poki-toki" rows="4" cols="80"></textarea>
			<input type="submit" value="o pana">
		</form>
	</body>
</html>
