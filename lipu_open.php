<?php
session_start();

if ($_SESSION['ID']) {
	include_once 'o_open_e_poki_sona.php';

	$s = 'Latn';
	$n = [
		'nanpa musi' => [
			'Latn' => 'nanpa musi',
			'Qaap' => 'nanpa+musi'
		],
		'nimi toki' => [
			'Latn' => 'nimi toki',
			'Qaap' => 'nimi+toki'
		],
		'musi o' => [
			'Latn' => 'musi o!',
			'Qaap' => 'musi o'
		],
		'lukin e musi sina' => [
			'Latn' => 'o lukin e musi sina',
			'Qaap' => 'o lukin e musi+sina'
		],
		'lipu' => [
			'Latn' => 'lipu pi jan kepeken',
			'Qaap' => 'lipu pi+jan+kepeken'
		],
		'toki o' => [
			'Latn' => 'toki o!',
			'Qaap' => 'toki o'
		],
		'seme' => [
			'Latn' => 'musi li seme?',
			'Qaap' => 'musi li seme'
		]
	];
}
?>
<html>
	<?php include 'insa_insa.php' ?>
	<body>
	<?php if ($_SESSION['ID']): ?>
		<div style="float:left">
			<?php
			$seme_a = $poki_sona->query(<<<SQL
				SELECT
					nimi,
					tu.ID AS ID,
					COUNT(ID_pi_jan_kepeken) AS nanpa
				FROM kipisi_musi AS wan
				LEFT JOIN jan_kepeken AS tu ON wan.ID_pi_jan_kepeken=tu.ID
				GROUP BY ID_pi_jan_kepeken
				ORDER BY nanpa DESC
				LIMIT 10;
				SQL
			);

			echo "<table><tr><th>nimi</th><th>{$n['nanpa musi'][$s]}</th></tr>";

			$sina = false;
			while ($kipisi = $seme_a->fetch()) {
				if ($_SESSION['ID'] == $kipisi['ID']) {
					echo '<tr class="sina">';
					$sina = true;
				}
				else
					echo '<tr>';
				echo "<td>{$kipisi['nimi']}</td><td>{$kipisi['nanpa']}</td></tr>";
			}
			if (!$sina) {
				$seme_a = $poki_sona->prepare(<<<SQL
					SELECT COUNT(*)
					FROM kipisi_musi
					WHERE ID_pi_jan_kepeken=?;
					SQL
				);
				$seme_a->execute([$_SESSION['ID']]);
				echo "<tr><td colspan=\"2\">…</tr><tr><td>(sina)</td><td>{$seme_a->fetchColumn()}</td></tr>";
			}
			echo '</table>';
			?>
		</div>
		<div style="float:right">
			<?php
			$seme_a = $poki_sona->query(<<<SQL
				SELECT
					tu.nimi_toki AS nimi,
					wan.toki AS toki,
					COUNT(wan.toki) AS nanpa
				FROM kipisi_musi AS wan
				LEFT JOIN poki_toki AS tu ON wan.toki=tu.toki
				GROUP BY toki
				ORDER BY nanpa DESC
				LIMIT 10;
				SQL
			);

			echo "<table><tr><th>{$n['nimi toki'][$s]}</th><th>toki</th><th>{$n['nanpa musi'][$s]}</th></tr>";

			while ($kipisi = $seme_a->fetch()) {
				if ($kipisi['toki'] == 'qtp' or $kipisi['toki'] == 'und')
					continue;
				echo "<tr><td>{$kipisi['nimi']}</td><td>{$kipisi['toki']}</td><td>{$kipisi['nanpa']}</td></tr>";
			}
			echo '</table>';
			?>
		</div>
	<?php endif; ?>
		<img src="musi-pi-ilo-toki.png" alt="musi pi ilo toki!">
	<?php if ($_SESSION['ID']): ?>
		<p><a href="musi.php"><?= $n['musi o'][$s] ?></a></p>
		<p><a href="lukin_musi.php?jan=<?= $_SESSION['ID']; ?>"><?= $n['lukin e musi sina'][$s] ?></a></p>
		<p><a href="lipu_pi_jan_kepeken.php"><?= $n['lipu'][$s] ?></a></p>
		<p><a href="seme.php"><?= $n['seme'][$s] ?></a></p>
	<?php else: ?>
		<p><a href="open_pi_jan_kepeken.php">toki o!</a></p>
		<p><a href="seme.php">musi li seme?</a></p>
	<?php endif;
	include 'anpa.php'; ?>

	</body>
</html>
