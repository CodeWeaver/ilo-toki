<?php
session_start();
require_once 'o_open_e_poki_sona.php';

switch ($_POST['pakala']) {
	case 'nimi jaki':
		$seme_a = $poki_sona->prepare('SELECT ID_musi FROM kipisi_musi WHERE ID=? LIMIT 1;');
		$seme_a->execute([$_POST['kipisi']]);
		$ID_musi = $seme_a->fetchColumn();

		$seme_a = $poki_sona->prepare(<<<SQL
			UPDATE kipisi_musi
			SET jaki = jaki+1
			WHERE ID=?;
			SQL
		);
		$seme_a->execute([$_POST['kipisi']]);
		$seme_a = $poki_sona->prepare(<<<SQL
			INSERT INTO jaki_tawa_jan_kepeken
			(ID_pi_jan_kepeken,ID_musi) VALUES (?,?);
			SQL
		);
		$seme_a->execute([$_SESSION['ID'],$ID_musi]);

		// kipisi ni li sin sin, kin ni li jaki mute la, o moli e kipisi ni
		$seme_a = $poki_sona->prepare(<<<SQL
			SELECT ID,jaki
			FROM kipisi_musi
			WHERE ID_musi=?
			ORDER BY ID
			LIMIT 1;
			SQL
		);
		$seme_a->execute([$ID_musi]);
		$kipisi = $seme_a->fetch();

		$jaki = 2;
		if ($kipisi['jaki'] > $jaki) {
			$seme_a = $poki_sona->prepare(<<<SQL
				DELETE FROM kipisi_musi
				WHERE ID=?;
				SQL
			);
			$seme_a->execute([$_POST['kipisi']]);
		}
		break;
	case 'toki pakala':
		$seme_a = $poki_sona->prepare(<<<SQL
			UPDATE kipisi_musi
			SET toki = 'und'
			WHERE ID=?;
			SQL
		);
		$seme_a->execute([$_POST['kipisi']]);
		$seme_a = $poki_sona->prepare('SELECT ID_musi FROM kipisi_musi WHERE ID=? LIMIT 1;');
		$seme_a->execute([$_POST['kipisi']]);
		$ID_musi = $seme_a->fetchColumn();
		$seme_a = $poki_sona->prepare(<<<SQL
			UPDATE musi
			SET toki_lon = 'und'
			WHERE ID=?;
			SQL
		);
		$seme_a->execute([$ID_musi]);
		break;
}

header('Location: lipu_open.php', true, 303);
exit();
?>
